import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueueTest {

    private BlockingQueue<String> blockingQueue = new PriorityBlockingQueue<String>();

    private void write() {
        try {
            Scanner scan = new Scanner(System.in);
            String text;
            while (true) {
                System.out.println("Write: ");
                text = scan.nextLine();
                blockingQueue.add(text);
                if (text.equals("exit")) {
                    System.exit(0);
                }
                Thread.sleep(20);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void read() {
        try {
            while (true) {
                String text = blockingQueue.take();
                System.out.println("Read: " + text);
                if(text.equals("exit")) {
                    System.exit(0);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void action() throws InterruptedException {
        Thread writeT = new Thread(this::write);
        Thread readT = new Thread(this::read);

        writeT.start();
        readT.start();

        writeT.join();
        readT.join();
    }

    public static void main(String[] args) throws InterruptedException {
        BlockingQueueTest test = new BlockingQueueTest();
        test.action();
    }
}
