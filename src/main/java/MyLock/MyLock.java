package MyLock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyLock {

    public void doLock(Lock lock) {
        lock.lock();
        System.out.println(Thread.currentThread().getName() + " is locked");

        lock.unlock();
        System.out.println(Thread.currentThread().getName() + " is unlocked");
    }

    public void test() {
        System.out.println("Locking test");
        MyLock myLock = new MyLock();

        Lock lock1 = new ReentrantLock();

        new Thread(() -> myLock.doLock(lock1)).start();
    }

    public static void main(String[] args) {
        MyLock lock = new MyLock();
        lock.test();
    }
}
